import json, os


# reads the json file
def parse_json(file):
    with open(file, "r") as data:
        try:
            json_data = json.load(data)
            return json_data
        except:
            return []


# retrieves environment variable 
def get_env(name):
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)