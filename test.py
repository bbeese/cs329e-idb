import os, sys
import unittest
from app import create_app
from models import db, Book, Author, Publisher

# dialect+driver://username:password@host:port/database


class DB_Tests(unittest.TestCase):
    app = create_app()
    app.app_context().push()

    # Tests our system can insert a new book successfully
    def test1(self):
        testValue = Book(isbn = "1234", title = "TEST1")
        db.session.add(testValue)
        db.session.commit()

        q = Book.query.filter_by(isbn = "1234").first()
        self.assertEqual(str(q.isbn), "1234")

        Book.query.filter_by(isbn = "1234").delete()
        db.session.commit()

    # Tests our system can handle adding a new author
    def test2(self):
        testValue = Author(name = "name")
        db.session.add(testValue)
        db.session.commit()

        q = Author.query.filter_by(name = "name").first()
        self.assertEqual(str(q.name), "name")

        Author.query.filter_by(name = "name").delete()
        db.session.commit()

    # Tests our system can handle adding a new publisher
    def test3(self):
        testValue = Publisher(name = "pub")
        db.session.add(testValue)
        db.session.commit()

        q = Publisher.query.filter_by(name = "pub").first()
        self.assertEqual(str(q.name), "pub")

        Publisher.query.filter_by(name = "pub").delete()
        db.session.commit()


if __name__ == '__main__':
    unittest.main()

