from flask import url_for


# Object used to simplify information fed into the flask application for the about page
class Person:
    def __init__(self, name, bio, image, responsibilities, commits = 0, issues = 0, unit_tests = 0):
        self.name = name
        self.bio = bio
        self.image = url_for('static', filename='images/' + image)
        self.responsibilities = responsibilities
        self.commits = commits
        self.issues = issues
        self.unit_tests = unit_tests