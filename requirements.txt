astroid==1.4.9
autopep8==1.2.4
Click==7.0
coverage==4.2
Flask==1.0.2
Flask-SQLAlchemy==2.3.2
gunicorn==19.7.1
isort==4.3.4
itsdangerous==1.1.0
Jinja2==2.10
lazy-object-proxy==1.3.1
MarkupSafe==1.0
mccabe==0.6.1
pep8==1.7.0
psycopg2==2.7.5
pylint==1.6.5
requests==2.13.0
six==1.11.0
SQLAlchemy==1.2.12
SQLAlchemy-Utils==0.33.6
virtualenv==15.1.0
Werkzeug==0.14.1
wrapt==1.10.11
