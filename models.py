from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

# global helper tables
# easiest to implement each model as a primary key to every other model, though not best practice

book_authors = db.Table('book_authors',
    db.Column('book_id', db.Integer, db.ForeignKey('books.id'), primary_key=True),
    db.Column('author_id', db.Integer, db.ForeignKey('authors.id'), primary_key=True)
)

book_publishers = db.Table('book_publishers',
    db.Column('book_id', db.Integer, db.ForeignKey('books.id'), primary_key=True),
    db.Column('publisher_id', db.Integer, db.ForeignKey('publishers.id'), primary_key=True)
)

author_books = db.Table('author_books',
    db.Column('book_id', db.Integer, db.ForeignKey('books.id'), primary_key=True),
    db.Column('author_id', db.Integer, db.ForeignKey('authors.id'), primary_key=True)
)

author_publishers = db.Table('author_publishers',
    db.Column('author_id', db.Integer, db.ForeignKey('authors.id'), primary_key=True),
    db.Column('publisher_id', db.Integer, db.ForeignKey('publishers.id'), primary_key=True)
)

publisher_books = db.Table('publisher_books',
    db.Column('book_id', db.Integer, db.ForeignKey('books.id'), primary_key=True),
    db.Column('publisher_id', db.Integer, db.ForeignKey('publishers.id'), primary_key=True)
)

publisher_authors = db.Table('publisher_authors',
    db.Column('author_id', db.Integer, db.ForeignKey('authors.id'), primary_key=True),
    db.Column('publisher_id', db.Integer, db.ForeignKey('publishers.id'), primary_key=True)
)


# inherits from db.Model
class BaseModel(db.Model):
    __abstract__ = True

    def __init__(self, *args):
        super().__init__(*args)

    def __repr__(self):
        return "<id {}>".format(self.id)


# Below are classes for each pillar; Book, Author, Publisher

# Create a pillar for Books
class Book(BaseModel, db.Model):
    __tablename__ = "books"
    id = db.Column(db.Integer, primary_key=True, nullable = False)
    google_id = db.Column(db.String(), nullable = True)
    title = db.Column(db.String(), nullable = True)
    isbn = db.Column(db.String(), nullable = True)
    publication_date = db.Column(db.String(), nullable = True)
    image_url = db.Column(db.String(), nullable = True)
    description = db.Column(db.String(), nullable = True)

    book_authors = db.relationship(
        'Author',
        secondary = book_authors,
        lazy = "subquery",
        backref = db.backref("book_authors", lazy = True)
    )

    book_publishers = db.relationship(
        'Publisher',
        secondary = book_publishers,
        lazy = "subquery",
        backref = db.backref("book_publisher", lazy = True)
    )

    # initializing Book class, retrieve necessary information
    def __init__(self, **kwargs):
        self.title = kwargs.get('title')
        self.google_id = kwargs.get('google_id')
        self.isbn = kwargs.get('isbn')
        self.publication_date = kwargs.get('publication_date')
        self.image_url = kwargs.get('image_url')
        self.description = kwargs.get('description')

# Create a pillar for Authors
class Author(BaseModel, db.Model):
    __tablename__ = "authors"
    id = db.Column(db.Integer, primary_key = True, nullable = False)
    born = db.Column(db.String(), nullable = True)
    name = db.Column(db.String(), nullable = True)
    education = db.Column(db.String(), nullable = True)
    description = db.Column(db.String(), nullable = True)
    nationality = db.Column(db.String(), nullable = True)
    alma_mater = db.Column(db.String(), nullable = True)
    wikipedia_url = db.Column(db.String(), nullable = True)
    image_url = db.Column(db.String(), nullable = True)

    author_books = db.relationship(
        'Book',
        secondary = author_books,
        lazy = "subquery",
        backref = db.backref("author_books", lazy = True)
    )

    author_publishers = db.relationship(
        'Publisher',
        secondary = author_publishers,
        lazy = "subquery",
        backref = db.backref("author_publishers", lazy = True)
    )

    # initializing Author class, retrieve necessary information
    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.born = kwargs.get('born')
        self.education = kwargs.get('education')
        self.description = kwargs.get('description')
        self.nationality = kwargs.get('nationality')
        self.image_url = kwargs.get('image_url')
        self.alma_mater = kwargs.get('alma_mater')
        self.wikipedia_url = kwargs.get('wikipedia_url')

# create a pillar for Publishers
class Publisher(BaseModel, db.Model):
    __tablename__ = "publishers"
    id = db.Column(db.Integer, primary_key=True, nullable = False)
    wikipedia_url = db.Column(db.String(), nullable = True)
    name = db.Column(db.String(), nullable = True)
    description = db.Column(db.String(), nullable = True)
    owner = db.Column(db.String(), nullable = True)
    location = db.Column(db.String(), nullable = True)
    image_url = db.Column(db.String(), nullable = True)
    website = db.Column(db.String(), nullable = True)
    parent_company = db.Column(db.String(), nullable = True)

    publisher_authors = db.relationship(
        'Author',
        secondary = publisher_authors,
        lazy = "subquery",
        backref = db.backref("publisher_authors", lazy = True)
    )

    publisher_books = db.relationship(
        'Book',
        secondary = publisher_books,
        lazy = "subquery",
        backref = db.backref("publisher_books", lazy = True)
    )

    # initializing Publisher class, retrieve necessary information
    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.description = kwargs.get('description')
        self.owner = kwargs.get('owner')
        self.image_url = kwargs.get('image_url')
        self.location = kwargs.get('location')
        self.website = kwargs.get('website')
        self.parent_company = kwargs.get('parent_company')
        self.wikipedia_url = kwargs.get('wikipedia_url')