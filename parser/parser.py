import json

books = []
authors = []
publishers = []

author_names = {}
publisher_names = {}


with open("complete.json", "r") as complete:
    data = json.load(complete)
    
    for book in data:
        book_id = len(books)
        book["id"] = book_id
        book_authors = []
        book_publishers = []
        meta = {"id": book_id, "title": book["title"]}

        for author in book["authors"]:
            if author["name"] in author_names:
                a_id = author_names[author["name"]] 
                a = authors[a_id]
                a["books"].append(meta)
                book_authors.append({"id": a_id, "name": author["name"]})
            else:
                author_id = len(authors)
                author["id"] = author_id
                author["books"] = [meta]
                author_names[author["name"]] = author_id
                authors.append(author)
                book_authors.append({"id": author_id, "name": author["name"]})
        

        
        for publisher in book["publishers"]:
            pub_id = len(publishers)
            if publisher["name"] in publisher_names:
                p_id = publisher_names[publisher["name"]]
                p = publishers[p_id]
                for ba in book_authors:
                    a_in = [a for a in p["authors"] if a["id"] == ba["id"]]
                    if len(a_in) == 0:
                        p["authors"].append(ba)
                p["books"].append(meta)
                book_publishers.append({"id": p_id, "name": publisher["name"]})
            else:
                publisher["id"] = pub_id
                publisher["books"] = [meta]
                publisher_names[publisher["name"]] = pub_id
                publisher["authors"] = book_authors
                publishers.append(publisher)
                book_publishers.append({"id": pub_id, "name": publisher["name"]})

        book["authors"] = book_authors
        book["publishers"] = book_publishers
        books.append(book)

"""with open("b.json", "w") as b:
    b.write(json.dumps(books))

with open("a.json", "w") as a:
    a.write(json.dumps(authors))"""

with open("p.json", "w") as p:
    p.write(json.dumps(publishers))
