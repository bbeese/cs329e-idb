import json, sys, subprocess, os
from flask import Flask, render_template, url_for, abort
from flask_sqlalchemy import SQLAlchemy
from psycopg2 import IntegrityError
from models import db, Author, Book, Publisher
from Person import Person
from utils import parse_json


# Initializes app, verifies credentials
def create_app():
    # allows for more flexible pre-creation setup
    app = Flask(__name__)
    
    if os.environ.get('GITLAB_CI') == 'true':
        app.config.update(
            POSTGRES_USER = "postgres",
            POSTGRES_PASSWORD = "postgres",
            POSTGRES_URL = "postgres",
            POSTGRES_DB = "bookdb"
        )
    else:    
        app.config.update(
            POSTGRES_USER = "group8books",
            POSTGRES_PASSWORD = "group8books",
            POSTGRES_URL = 'localhost', # 127.0.0.1:5432
            POSTGRES_DB = "bookdb"
        )

    print("app", os.environ.get('IN_APP_ENGINE'))
    if os.environ.get('IN_APP_ENGINE') is None:
        os.environ["DB_URL"] = "postgresql+psycopg2://{user}:{pw}@{url}/{db}".format(
            user = app.config["POSTGRES_USER"],
            pw = app.config["POSTGRES_PASSWORD"],
            url = app.config["POSTGRES_URL"],
            db = app.config["POSTGRES_DB"]
        )
        app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["DB_URL"]
    else:
        app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql+psycopg2://group8books:group8books@/bookdb?host=/cloudsql/bamboo-life-220417:us-central1:group8books"

    print(app.config["SQLALCHEMY_DATABASE_URI"])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    
    db.init_app(app)
    return app


app = create_app()

# Generic error handler
@app.errorhandler(404)
def not_found(error):
    return render_template("404.html"), 404


# Home page template
@app.route("/")
def index():
    image = url_for('static', filename='images/bookshelf.jpg')
    return render_template("index.html", image = image)


# Information for each group member, displayed on the about page
@app.route("/about")
def about():
    # alphabetized
    members = [
        Person("Salome Abekhe", "Salome is a senior Economics major and an aspiring Data Analyst. She recently returned from a semester in South Korea where she closely studied Korea's Economic Catch-up and Technological Leapfrogging over the years. ", "salome_abekhe.jpg", "Operations, Deployment", 20, 11, 1),
        Person("Brett Beese", "Brett is a senior Mathematics major from Cedar Park, Texas, who recently completed his first professional software engineering experience as an intern at Cylance.", "brett_beese.jpg", "Organization, frontend", 18, 8, 1),
        Person("Austin Clark", "Austin is a fifth year computational biology major from New Braunfels, Texas. He also works as an engineering instructor at Hello World Studio.", "austinclark.jpg", "front-end, design", 21, 12, 1),
        Person("Maheep Myheni", "Maheep is a junior Mathematics major from San Antonio, Texas. He enjoys being active through sports, the great outdoors, and volunteering in the community. ", "maheepmyneni.jpg", "models, overseeing technical report", 22, 9, 0),
        Person("Junyuan Tan", "Jun is a fourth year Plan II and government major from Houston, Texas. He has over three years of professional software engineering experience, most recently at J.P. Morgan in New York where he worked on trading systems.", "junyuan_tan.jpg", "Flask, front-end, design", 15, 5, 0)
    ]

    return render_template("about.html", members = members)


# first define a collection view, and then specify down based on item ID
# Returns all books
@app.route("/books")
def books():
    books = Book.query.all()
    return render_template("items.html", title = "Books", items = books)


# Returns desired book information
@app.route("/books/<id>")
def get_book(id):
    id = int(id)
    book = Book.query.get_or_404(id)
    return render_template("item.html", title = book.title, item = book)


# Returns all authors
@app.route("/authors")
def authors():
    authors = Author.query.all()
    return render_template("items.html", title = "Authors", items = authors)


# Returns desired author information
@app.route("/authors/<id>")
def get_author(id):
    id = int(id)
    author = Author.query.get_or_404(id)
    return render_template("item.html", title = author.name, item = author)


# Returns all publishers
@app.route("/publishers")
def publishers():
    publishers = Publisher.query.all()
    return render_template("items.html", title = "Publishers", items = publishers)


# Returns desired publisher information
@app.route("/publishers/<id>")
def get_publisher(id):
    id = int(id)
    publisher = Publisher.query.get_or_404(id)
    return render_template("item.html", title = publisher.name, item = publisher)


# Retrieves and runs the unit tests
@app.route("/test")
def get_test():
    p = subprocess.Popen(["python", "-m", "coverage", "run", "--branch", "test.py"],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                stdin=subprocess.PIPE)
    out, err = p.communicate()
    output = err + out
    output = output.decode("utf-8")

    return render_template("test.html", output = output)


# Resets database
@app.cli.command('resetdb')
def resetdb():
    from sqlalchemy_utils import database_exists, create_database, drop_database
    if database_exists(app.config["SQLALCHEMY_DATABASE_URI"]):
        print('Deleting database.')
        drop_database(app.config["SQLALCHEMY_DATABASE_URI"])
    if not database_exists(app.config["SQLALCHEMY_DATABASE_URI"]):
        print('Creating database.')
        create_database(app.config["SQLALCHEMY_DATABASE_URI"])

    print('Creating tables.')
    db.create_all()
    print('Done!')

# Seeds the database with our data
@app.cli.command("seeddb")
def seed_db():
    with open("parser/complete.json", "r") as complete:
        data = json.load(complete)
        
        for book in data:
            new_book = Book(
                title = book.get("title", ""),
                google_id = book.get("google_id", ""),
                isbn = book.get("isbn", ""),
                publication_date = book.get("publication_date", ""),
                image_url = book.get("image_url", ""),
                description = book.get("description", "")
            )

            for author in book["authors"]:
                author_in_db = Author.query.filter_by(name = author["name"]).first()
                
                if author_in_db is not None:
                    author_in_db.author_books.append(new_book)
                    try:
                        new_book.book_authors.append(author_in_db)
                    except IntegrityError:
                        print("Duplicate author", author_in_db.name, "on book", new_book.title)
                        continue
                else:
                    new_author = Author(
                        born = author.get("born", ""),
                        name = author.get("name", ""),
                        description = author.get("description", ""),
                        education = author.get("education", ""),
                        nationality = author.get("nationality", ""),
                        alma_mater = author.get("alma_mater", ""),
                        wipedia_url = author.get("wikipedia_url", ""),
                        image_url = author.get("image_url", "")
                    )
                    new_author.author_books.append(new_book)
                    try:
                        new_book.book_authors.append(new_author)
                    except IntegrityError:
                        print("Duplicate author", new_author.name, "on book", new_book.title)
                        continue
            

            
            for publisher in book["publishers"]:
                publisher_in_db = Publisher.query.filter_by(name = publisher["name"]).first()

                if publisher_in_db is not None:
                    new_book.book_publishers.append(publisher_in_db)
                    publisher_in_db.publisher_books.append(new_book)

                    pub_authors = Publisher.query.filter_by(name = publisher_in_db.name).first()

                    for a in new_book.book_authors:
                        if pub_authors is not None:
                            in_a = [author for author in pub_authors.publisher_authors if a.name == author.name]

                            if len(in_a) == 0:
                                try:
                                    publisher_in_db.publisher_authors.append(a)
                                except IntegrityError:
                                    print("Duplicate author", a.name, "on pub", publisher_in_db.name)
                                    continue
                                
                        else:
                            try:
                                publisher_in_db.publisher_authors.append(a)
                            except IntegrityError:
                                print("Duplicate author", a.name, "on pub", publisher_in_db.name)
                                continue
                else:
                    new_publisher = Publisher(
                        name = publisher.get("name", ""),
                        wikipedia_url = publisher.get("wikipedia_url", ""),
                        description = publisher.get("description", ""),
                        location = publisher.get("location", ""),
                        parent_company = publisher.get("parent company", ""),
                        owner = publisher.get("owner", ""),
                        image_url = publisher.get("image_url", ""),
                        website = publisher.get("website", "")
                    )

                    new_publisher.publisher_books.append(new_book)
                    new_book.book_publishers.append(new_publisher)


                    pub_authors = Publisher.query.filter_by(name = new_publisher.name).first()

                    for a in new_book.book_authors:
                        if pub_authors is not None:
                            in_a = [author for author in pub_authors.publisher_authors if a["name"] == author["name"]]

                            if len(in_a) == 0:
                                try:
                                    new_publisher.publisher_authors.append(a)
                                except IntegrityError:
                                    print("Duplicate author", a.name, "on pub", new_publisher.name)
                                    continue
                                
                        else:
                            try:
                                new_publisher.publisher_authors.append(a)
                            except IntegrityError:
                                print("Duplicate author", a.name, "on pub", new_publisher.name)
                                continue
            print(new_book.title, "added!")
            db.session.add(new_book)
        db.session.commit()
        print("Done!")